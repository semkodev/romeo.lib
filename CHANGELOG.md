# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.0] - 2018-10-03

### Added

* Multi-account support for Ledger

## [0.4.5] - 2018-10-03

### Changed

* Multiple performance improvements and fixes

## [0.2.1] - 2018-09-30

### Changed

* Fixes
* Ledger support
* Performance improvements

## [0.2.0] - 2018-09-30

### Changed

* Fixes
* Ledger support
* Performance improvements

## [0.1.7] - 2018-09-03

### Changed

* Rebranding
* fix CI

## [0.1.1] - 2018-06-12

### Changed

* Change repo URL
* New project/package signature
